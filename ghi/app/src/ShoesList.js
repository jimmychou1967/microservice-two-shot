import React from 'react';




function ShoeList(props) {
  const deleteShoe = async (href) => {
    fetch(`http://localhost:8080${href}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(() => {
      window.location.reload();
  })
  }
  

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>color</th>
            <th>Bin</th>            
            <th>Remove</th>            
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            console.log(shoe)
            return (
              <tr key={shoe.href}>  
                <td><img src={shoe.picture_url} alt=''width="20%" height="20%"/></td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin }</td>
                <td><button onClick={() => deleteShoe(shoe.href)} type="button" className="btn btn-danger">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default ShoeList;
  